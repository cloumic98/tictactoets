import Game from "../core/Game";

export default class Button {

    private button: HTMLButtonElement
    private playerClicked: string = ""

    constructor() {
        this.button = document.createElement("button")
        this.playerClicked = ""

        this.button.addEventListener("click", this.onclick)
    }

    onclick(event: Event) {
        let btn = event.target as HTMLButtonElement

        if(btn.innerHTML == "") {
            btn.innerHTML = Game.player ? "X" : "O"
            Game.button[parseInt(btn.id)].setPlayerClicked(btn.innerHTML)
            Game.player = !Game.player

            Game.checkWin()
        }
    }

    setDisabled(): void {
        this.button.disabled = true
    }

    getPlayerClicked(): string {
        return this.playerClicked
    }

    setPlayerClicked(player: string): void {
        this.playerClicked = player
    }

    getId(): string {
        return this.button.id
    }

    setId(id: number): void {
        this.button.id = id.toString()
    }

    setParent(div: HTMLDivElement) {
        div.appendChild(this.button)
    }
}
