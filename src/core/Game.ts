import Button from "../utils/Button";

enum State {
    PLAYING,
    FINISHED
}

export default class Game {

    static player: boolean = true

    static button: Button[] = []

    static state: State = State.PLAYING

    static checkWin() {

        if(Game.state == State.FINISHED) {
            return
        }

        if(
            (Game.button[0].getPlayerClicked() != "" && Game.button[0].getPlayerClicked() == Game.button[1].getPlayerClicked() && Game.button[1].getPlayerClicked() == Game.button[2].getPlayerClicked())||
            (Game.button[3].getPlayerClicked() != "" && Game.button[3].getPlayerClicked() == Game.button[4].getPlayerClicked() && Game.button[4].getPlayerClicked() == Game.button[5].getPlayerClicked())||
            (Game.button[6].getPlayerClicked() != "" && Game.button[6].getPlayerClicked() == Game.button[7].getPlayerClicked() && Game.button[7].getPlayerClicked() == Game.button[8].getPlayerClicked())||
            (Game.button[0].getPlayerClicked() != "" && Game.button[0].getPlayerClicked() == Game.button[3].getPlayerClicked() && Game.button[3].getPlayerClicked() == Game.button[6].getPlayerClicked())||
            (Game.button[1].getPlayerClicked() != "" && Game.button[1].getPlayerClicked() == Game.button[4].getPlayerClicked() && Game.button[4].getPlayerClicked() == Game.button[7].getPlayerClicked())||
            (Game.button[2].getPlayerClicked() != "" && Game.button[2].getPlayerClicked() == Game.button[5].getPlayerClicked() && Game.button[5].getPlayerClicked() == Game.button[8].getPlayerClicked())||
            (Game.button[0].getPlayerClicked() != "" && Game.button[0].getPlayerClicked() == Game.button[4].getPlayerClicked() && Game.button[4].getPlayerClicked() == Game.button[8].getPlayerClicked())||
            (Game.button[2].getPlayerClicked() != "" && Game.button[2].getPlayerClicked() == Game.button[4].getPlayerClicked() && Game.button[4].getPlayerClicked() == Game.button[6].getPlayerClicked())
        ) {
            Game.state = State.FINISHED
            Game.button.forEach((btn) => {
                btn.setDisabled()
            })

            document.getElementById("winner")!.innerHTML = "Le joueur "+ (!Game.player ? "Bastien" : "Matthieu") + " à gagner"
        }
    }

    generateGrid(app: HTMLDivElement) {
        let idCounter: number = 0
        let div: HTMLDivElement

        for(let i: number = 0; i < 3; i++) {
            div = app.children[i] as HTMLDivElement

            for(let j: number = 0; j < 3; j++) {
                let btn: Button = new Button()
                btn.setId(idCounter)
                btn.setParent(div)
                Game.button.push(btn)

                idCounter++
            }
        }
    }
}
