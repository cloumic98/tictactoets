import './style.css'
import Game from "./core/Game";

const app = document.querySelector<HTMLDivElement>('#app')!

const game: Game = new Game()

game.generateGrid(app)
